/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.stereotype.Component;

/**
 *
 * @author krawler
 */

@Aspect
@Component
@Configuration
@EnableAspectJAutoProxy(proxyTargetClass = true)
public class AspectClass {
    
//    @Pointcut("execution(* com.mycompany.aoptest.*.*(..))")
//    public void test1(){}
    
    @After("execution(* com.mycompany.aoptest.*.test1())")
     public void myadvice(JoinPoint jp)//it is advice (after advice)  
    {   
        System.out.println("AOP Running");  
    }   
}
