/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.aoptest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

/**
 *
 * @author krawler
 */


public class MainClass {
    
//@Autowired
//private TopClass topClass=new TopClass();

    public static void main(String args[]){
        ConfigurableApplicationContext applicationContext = new AnnotationConfigApplicationContext(AppConfig.class);
        TopClass top = applicationContext.getBean(TopClass.class);
//        MainClass m=new MainClass();
//        m.testmain();
        top.test1();
        System.out.println("Running Main Method");
    }
//   public void testmain(){
//        topClass.test1();
//    }
    
}
