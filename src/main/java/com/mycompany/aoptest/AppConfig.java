/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.aoptest;



import com.mycompany.aspect.AspectClass;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

/**
 *
 * @author krawler
 */
 @Configuration
 @EnableAspectJAutoProxy(proxyTargetClass = true)
 @ComponentScan(basePackages={"com.mycompany.aspect", "com.mycompany.aoptest"})
 public class AppConfig {
     @Bean
     public TopClass topClass() {
         return new TopClass();
     }
//     @Bean
//     public AspectClass myaspectClass(){
//         return new AspectClass();
//     }
             

     
 }
